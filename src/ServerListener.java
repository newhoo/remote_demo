import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerListener {
    private static Socket socket;
    private static ObjectInputStream OIS;
    private static ObjectOutputStream OOS;
    private static JLabel IMG_LAB;
    private static boolean CONNECTED;

    public static void main(String[] args) throws IOException {
        int port = 7777;
        if (args.length > 0)
            port = Integer.parseInt(args[0]);
        try {
            showUI();
            openServer(port);

            while (true) {
                receivePic();
            }
        } catch (Exception ee) {
            OIS.close();
            socket.close();
        }

    }

    public static void openServer(int port) throws IOException {
        System.out.println("ServerStart.....");
        ServerSocket server = new ServerSocket(port);
        socket = server.accept();
        System.out.println("连接上...\n" + socket);
        CONNECTED = true;
        OIS = new ObjectInputStream(socket.getInputStream());
        OOS = new ObjectOutputStream(socket.getOutputStream());
    }

    public static void receivePic() throws ClassNotFoundException, IOException {
        Message g = (Message) OIS.readObject();
        BufferedImage bi = ImageIO.read(new ByteArrayInputStream(g.getFileContent()));
        ImageIcon iic = new ImageIcon(bi);

        Image img = iic.getImage();
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();

        int w = d.width;
        int h = d.height;
        BufferedImage bi2 = resize(img, (int) (w * 0.6), (int) (h * 0.6));


        IMG_LAB.setIcon(new ImageIcon(bi2));
        IMG_LAB.repaint();//销掉以前画的背景
    }


    public static void showUI() {
        //控制台标题
        JFrame jf = new JFrame("控制台");
        //事件监听
        setListener(jf);
        //控制台大小
        jf.setSize(1000, 600);
        //imag_lab用于存放画面
        IMG_LAB = new JLabel();
        jf.add(IMG_LAB);
        //设置控制台可见
        jf.setVisible(true);
        //控制台置顶
        jf.setAlwaysOnTop(true);
        jf.setResizable(true);
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private static BufferedImage resize(Image img, int newW, int newH) {
        int w = img.getWidth(null);
        int h = img.getHeight(null);
        BufferedImage dImg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_BGR);
        Graphics2D g = dImg.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
        g.dispose();
        return dImg;
    }

    public static void setListener(JFrame frame) {
        //panel设置监听器
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                sendEventObject(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                sendEventObject(e);
            }

            @Override
            public void keyTyped(KeyEvent e) {
                sendEventObject(e);
            }
        });
        frame.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                sendEventObject(e);
            }
        });
        frame.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                sendEventObject(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                sendEventObject(e);
            }
        });
        frame.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendEventObject(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                sendEventObject(e);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                sendEventObject(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                sendEventObject(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                sendEventObject(e);
            }

        });
    }

    private static void sendEventObject(InputEvent event) {
        if (!CONNECTED)
            return;
        try {
            System.out.println("send");
            OOS.writeObject(event);
            OOS.flush();
        } catch (Exception ef) {
            ef.printStackTrace();
        }
    }
}
