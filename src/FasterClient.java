import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class FasterClient {
    private static Socket socket;
    private static ObjectOutputStream OOS;
    private static ObjectInputStream OIS;
    private static Robot robot;
    private static JButton BTN;

    public static void main(String[] args) {
        showUI();
    }

    public static void start(String ip, int port) throws IOException {
        try {
            StartConnection(ip, port);
            RobotThread robot = new RobotThread(OIS);
            Thread t = new Thread(robot, "robot");
            t.start();
            while (true) {
                CapturePic();
            }
        } catch (Exception a) {
            OOS.close();
            socket.close();
        }
    }

    public static void StartConnection(String ip, int port) throws IOException {
        socket = new Socket(ip, port);
        if (socket.isConnected()) {
            System.out.println("socket connected..." + socket);
        }
        OOS = new ObjectOutputStream(socket.getOutputStream());
        OIS = new ObjectInputStream(socket.getInputStream());
    }

    public static void CapturePic() throws AWTException, IOException {
        robot = new Robot();
        Message msg;
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dm = tk.getScreenSize();
        Robot robot = new Robot();
        for (int i = 0; i < 50; i++) {
            //截取指定大小的屏幕区域
            Rectangle rec = new Rectangle(0, 0, (int) dm.getWidth(), (int) dm.getHeight());
            BufferedImage bimage = robot.createScreenCapture(rec);
            //将图片保存到文件中
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bimage, "jpeg", baos);
            msg = new Message("screenshot" + i + ".jpeg", baos.size(), baos.toByteArray());

            System.out.println(msg.getFileName());
            System.out.println("send");
            OOS.writeObject(msg);
            OOS.flush();
        }
    }

    public static void showUI() {
        //控制台标题
        JFrame jf = new JFrame("客户端");
        //控制台大小
        jf.setSize(400, 300);
        JLabel jLabel1 = new JLabel("IP:");
        final JTextField ip = new JTextField("127.0.0.1");
        JLabel jLabel2 = new JLabel("port:");
        final JTextField port = new JTextField("7777");
        BTN = new JButton("开始共享屏幕");
        BTN.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BTN.setEnabled(false);
                new Thread(() -> {
                    try {
                        start(ip.getText(), Integer.parseInt(port.getText()));
                    } catch (Exception e1) {
                        BTN.setEnabled(true);
                    }
                }).start();
            }
        });
        jf.add(jLabel1);
        jf.add(ip);
        jf.add(jLabel2);
        jf.add(port);
        jf.add(BTN);
        FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT);
        jf.setLayout(flowLayout);
        //设置控制台可见
        jf.setVisible(true);
        //控制台置顶
        jf.setAlwaysOnTop(true);
        jf.setResizable(true);
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
